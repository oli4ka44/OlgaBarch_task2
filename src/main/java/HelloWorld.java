import java.util.Scanner;
public class HelloWorld {
    public static void main(String[] agrs) {
        Scanner input = new Scanner(System.in);
        System.out.println("введите длину массива");
        int size = input.nextInt();
        System.out.printf("size = %d \n", size);
        int mass[] = new int[size];
        System.out.println("введите элементы через enter");

        for (int i = 0; i < size; i++) {
            mass[i] = input.nextInt();
        }
        System.out.println("ваш массив:");
        for (int i = 0; i < size; i++) {
            System.out.print(" " + mass[i]);
        }
        Scanner input2 = new Scanner(System.in);
        System.out.println("\n Выберите действие:\n" + "1) Расчет суммы всех элементов массива.\n" +
                "2) Нахождение максимального элемента массива.\n" +
                "3) Нахождение минимального элемента массива.\n" +
                "4) Вычисление среднего арифметического элементов массива.\n" +
                "5) Вычисление количества элементов массива, мЕньших среднего арифметического.\n" +
                "6) Вычисление количества элементов массива, бОльших среднего арифметического.\n" +
                "7) Подсчет количества четных элементов массива.\n" +
                "8) Подсчет количества нечетных элементов массива.\n" +
                "9) Сортировка массива.\n ");

        int z = input2.nextInt();
        if (z == 1 || z == 4 || z == 5 || z == 6) {
            float sum = 0;
            float average = 0;
            int count = 0;
            for (int i = 0; i < size; i++) {
                sum = sum + mass[i];
            }
            if (z == 1) {
                System.out.print("sum:" + sum);
            }
            average = sum / size;
            if (z == 4) {
                System.out.print("среднее:" + average);
            }
            if (z == 5) {
                for (int i = 0; i < size; i++) {
                    if (mass[i] < average) {
                        count += 1;
                    }
                }
                System.out.print("количество элементов массива, мЕньших среднего арифметического:" + count);
            }
            if (z == 6) {
                for (int i = 0; i < size; i++) {
                    if (mass[i] > average) {
                        count += 1;
                    }
                }
                System.out.print("количество элементов массива, бОльших среднего арифметического:" + count);
            }
        }
        if (z == 2 || z == 3) {
            int max = 0;
            for (int i = 0; i < size; i++) {
                if (mass[i] > max) {
                    max = mass[i];
                }
            }
            if (z == 2) System.out.print("max:" + max);
            int min = max;
            for (int i = 0; i < size; i++) {
                if (mass[i] < min) {
                    min = mass[i];
                }
            }
            if (z == 3) System.out.print("min:" + min);
        }
        if (z == 7 || z == 8) {
            int count1 = 0;
            int count2 = 0;
            for (int i = 0; i < size; i++) {
                if (mass[i] % 2 != 0) {
                    count1 += 1;
                } else {
                    count2 += 1;
                }
            }
            if (z == 7) System.out.print("количество четных элементов массива:" + count2);
            if (z == 8) System.out.print("количество нечетных элементов массива:" + count1);
        }
        if (z == 9) {
            boolean sort = false;
            int buf;
            while (!sort) {
                sort = true;
                for (int i = 0; i < size-1; i++) {
                    if (mass[i] < mass[i + 1]) {
                        sort = false;
                        buf = mass[i];
                        mass[i] = mass[i + 1];
                        mass[i + 1] = buf;
                    }
                }
            }
            System.out.println("ваш отсортированный массив:");
            for (int i = 0; i < size; i++) {
                System.out.print(" " + mass[i]);
            }
        }
    }
}

